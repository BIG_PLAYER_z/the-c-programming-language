#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SILENCE_NONCONFORMING_TGMATH_H
#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<Windows.h>
#include<stdbool.h>
#include<locale.h>
#include<time.h>
#include<string.h>
#include<assert.h>
#include<ctype.h>
#include<tgmath.h>
#include<stddef.h>