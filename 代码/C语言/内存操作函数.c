#include"源.h"

//void * memcpy (const void * str1, const void * str2, size_t num );
//memcpy - 比较ptr1和ptr2指针指向的位置开始，向后的num个字节 - 大于返回一个大于0的值，相等返回0，小于返回一个小于0的值
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7 };
//	int arr2[] = { 1,2,3,4,5,6,6 };
//	int ret1=memcmp(arr1, arr2, 24);
//	int ret2 = memcmp(arr1, arr2, 25);
//	printf("%d\n", ret1);
//	printf("%d\n", ret2);
//	return 0;
//}



//void * memset ( void * ptr, int value, size_t num );
//memset - 用来设置内存，将内存中的值以字节为单位设置成想要的内容
//
//int main()
//{
//	char arr1[] = "Hello world";
//	int arr2[5] = { 1,2,3,4,5 };
//	memset(arr1, 'x', 5);
//	memset(arr2, 1, 20);//以字节为单位设置 - 整型做不到以字节为单位设置
//	printf("%s\n", arr1);
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}


//void * memmove ( void * destination, const void * source, size_t num );
//memmove - 如果源空间和目标空间重叠，则使用memmove
// 
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	if (dest < src)
//	{
//		//前-->后
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		//后-->前
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr1 + 2, arr1, 5 * sizeof(int));
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}
//
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	memmove(arr1 + 2, arr1, 5*sizeof(int));
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}


//int main()
//{
//	int arr1[20] = { 1,2,3,4,5,6,7,8,9,10 };
//	memcpy(arr1 + 2,arr1,20);//dest和src有任何的重叠，复制的结果都是未定义的
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}


//void * memcpy ( void * destination, const void * source, size_t num );
//memcpy - 从sourse位置开始向后复制num个字节的数据到destination指向的内存位置，在遇到\0时不会停下来 - 针对内存块进行拷贝
//         memcpy函数拷贝结束后，会返回目标空间的起始地址
//		   memcpy不负责内存重叠的问题
//
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	int i = 0;
//	for (i = 0; i < num; i++)
//	{
//		//使用char*类型指针解引用一次访问1个字节
//		*(char*)dest = *(char*)src;
//		((char*)dest)++;
//		((char*)src)++;
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	my_memcpy(arr2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}
// 
// 
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	memcpy(arr2, arr1, 20);
//	int i = 0;
//	for (i = 0;i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}