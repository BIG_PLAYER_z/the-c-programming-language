#include"源.h"

//char * strerror ( int errnum );
//strerror - 可以把参数部分错误码对应的错误信息的字符串地址返回来
//void perror ( const char * str );
//perror - 有能力直接打印错误信息，打印时先打印传给perror的字符串，然后打印冒号，然后打印空格，最后打印错误码对应的错误信息
//         perror==printf+strerror
//
//int main()
//{
//	//fopen以读的形式打开文件的时候，如果文件不存在，就打开失败
//	FILE* pf = fopen("text.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		perror("wzz");
//		return 1;
//	}
//	//读文件
//
//	//关闭文件
//	fclose(pf);
//	return 0;
//}
// 
// 
//int main()
//{
//	int i = 0;
//	for(i=0;i<=10;i++)
//	{
//		printf("%s\n", strerror(i));
//	}
//	return 0;
//}


//char * strtok ( char * str, const char * sep );
//strtok - 提取由分隔符隔开的字符串 - 找到str中的下一个标记，并将其用\0结尾，返回一个指向这个标记的指针
// 
//int main()
//{
//	char arr1[] = "1173587181@qq.com";
//	char arr2[30] = { 0 };
//	strcpy(arr2, arr1);
//	const char* sep = "@.";//分隔符无顺序要求，要全
//	char* ret = NULL;
//	//ret = strtok(arr2, sep);
//	//printf("%s\n", ret);
//	//ret = strtok(NULL, sep);//若第一个参数是空指针，则从上一个标记处开始寻找
//	//printf("%s\n", ret);	
//	//ret = strtok(NULL, sep);
//	//printf("%s\n", ret);
//	for (ret=strtok(arr2, sep);ret!=NULL;ret=strtok(NULL,sep))
//	{
//		printf("%s\n",ret);
//	}
//	return 0;
//}


//简单写法 - 暴力查找
//KMP算法 - 在字符串中查找字符串

//char* my_strstr(const char* str1, const char* str2)
//{
//	const char* s1 = NULL;
//	const char* s2 = NULL;
//	const char* cur = str1;
//	if (*str2 == '\0')
//	{
//		return (char*)str1;
//	}
//	while (*cur)
//	{
//		s1 = cur;
//		s2 = str2;
//		while (*s1!='\0'&&*s2!='\0'&& *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return (char*)cur;
//		}
//		cur++;
//	}
//	return NULL;
//}
//
//int main()
//{
//	char arr1[] = "abcccdefgabcdefg";
//	char arr2[] = "cdef";
//	char*ret=my_strstr(arr1,arr2);
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);
//	}
//	else
//	{
//		printf("NULL\n");
//	}
//	return 0;
//}


//strstr - 在字符串str1中查找str2，若找到则返回str2第一次出现的位置，反之，返回空指针
//int main()
//{
//	char arr1[] = "This is an apple.\n";
//	const char* p = "is";
//	char* ret = strstr(arr1, p);
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);
//	}
//	else
//	{
//		printf("没找到\n");
//	}
//	return 0;
//}


//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "abcc";
//	int ret=strncmp(arr1, arr2, 3);
//	printf("%d\n", ret);
//	return 0;
//}


//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = "xxx\0xxxxxxxxxxxx";
//	char arr3[20]="xxx";
//	strncat(arr2, arr1, 3);//会追加'\0'
//	strncat(arr3, arr1, 8);
//	printf("%s\n", arr2);
//	printf("%s\n", arr3);
//	return 0;
//}


//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = "xxxxxxxxxxx";
//	char arr3[20] = "xxxxxxxxxxx";
//	strncpy(arr2, arr1, 3);
//	strncpy(arr3, arr1, 8);//若原字符串长度小于num，则补加\0直到字符数量等于num
//	printf("%s\n", arr2);
//	printf("%s\n", arr3);
//	return 0;
//}

//strcpy,strcat,strcmp - 长度不受限制的字符串函数
//strncpy,strncat,strncmp - 长度受限制的字符串函数 

//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//		{
//			return 0;
//		}
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2;
//}
//
//int main()
//{
//	char arr1[] = "abcdefz";
//	char arr2[] = "abcdef";
//	int ret = my_strcmp(arr1, arr2);
//	printf("%d\n",ret);
//	return 0;
//}


//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "abcdef";
//	char arr3[] = "abcc";
//	int ret1 = strcmp(arr1, arr3);//strcmp - 比较两个字符串的内容 - >返回大于0的值；==返回0；<返回小于0的值
//	int ret2 = strcmp(arr1, arr2);
//	printf("%d\n", ret1);
//	printf("%d\n", ret2);
//	return 0;
//}


//char* my_strcat(char* dest, const char* src)
//{
//	assert(dest && src);//若有一个NULL，则为0，&&为假
//	char* ret = dest;
//	//1.找到目标空间的\0
//	while (*dest != '\0')
//	{
//		dest++;
//	}
//	//2.拷贝
//	while (*dest++ = *src++)
//	{
//		;//空语句
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "Hello ";
//	char arr2[] = "world";
//	char*s=my_strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	printf("%s\n", s);
//	printf("%s\n", my_strcat(arr1, arr2));
//	return 0;
//}


//int main()
//{
//	char arr1[20] = "Hello ";
//	char arr2[] = "world";
//	strcat(arr1, arr2);//strcat - 用于链接或追加字符串
//	printf("%s\n", arr1);
//	return 0;
//}


//int my_strcpy(char* dest,const char*src)
//{
//	assert(src != NULL);
//	assert(dest != NULL);
//	char* ret = dest;
//	while (*dest++ = *src++)//'\0'的ASCII码值为0
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[] = "Hello world!";
//	char arr2[20] = "xxxxxxxxxxxxxxxxxxx";
//	char* ret=my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	printf("%s\n", ret);
//	return 0;
//}


//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[20] = { 0 };
//	strcpy(arr2, arr1);         //strcpy(目的地,源头)
//	printf("%s\n", arr2);
//	return 0;
//}


//size_t my_strlen1(const char* s)
//{
//	assert(s);
//	int count = 0;
//	while(*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}
////不创建临时变量，求字符串长度 —— 递归
//size_t my_strlen2(const char* s)
//{
//	if (*s == '\0')
//		return 0;
//	else
//	return 1 + my_strlen2(s + 1);
//}
//size_t my_strlen3(const char* s)
//{
//	assert(s != NULL);
//	char* ptr = s;
//	while (*s != '\0')
//	{
//		s++;
//	}
//	return s - ptr;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	size_t len1 = my_strlen1(arr);//计数器
//	size_t len2 = my_strlen2(arr);//递归
//	size_t len3 = my_strlen3(arr);//指针
//	printf("%zd\n", len1);
//	printf("%zd\n", len2);
//	printf("%zd\n", len3);
//	return 0;
//}


//int main()
//{
//	if (strlen("abc") - strlen("abcdef") > 0)//strlen的返回值是size_t类型，是无符号的
//	{
//		printf(">=\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	if ((int )strlen("abc") - (int )strlen("abcdef") > 0)
//	{
//		printf(">=\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}


//int main()
//{
//	char arr[] = "Hello World!";
//	int i = 0;
//	while (arr[i] != '\0')
//	{
//		arr[i] = toupper(arr[i]);
//		i++;
//	}
//	printf("%s\n", arr);
//
//	return 0;
//}


//int main()
//{
//	printf("%c\n", toupper('a'));//小写变大写，若本身是大写则不变
//	printf("%c\n", tolower('A'));//大写变小写，若本身是小写则不变
//	return 0;
//}


//int main()
//{
//	char arr[] =  "Hello World!" ;
//	int i = 0;
//	while (arr[i] != '\0')
//	{
//		if (islower(arr[i])!=0)
//		{
//			arr[i] -= 32;
//		}
//		i++;
//	}
//	printf("%s\n", arr);
//	
//	return 0;
//}


//int main()
//{
//	int ret1 = islower('A');//islower - 判断是否是小写字母:若是，返回非0值；反之返回0
//	int ret2 = isspace('\n');//isspace - 判断是否是空白字符:若是，返回非0值；反之返回0
//	printf("%d\n", ret1);
//	printf("%d\n", ret2);
//	return 0;
//}