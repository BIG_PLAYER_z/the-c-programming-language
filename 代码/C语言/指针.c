#include"源.h"


//int Num1(int num)
//{
//	int flag = num;
//	while (flag > 1)
//	{
//		num = num + flag / 2;
//		flag = flag / 2 + flag % 2;
//	}
//	return num;
//}
//int Num2(int num)
//{
//	static int more = 0;
//	more = num % 2;
//	if (more == 1 && num == 1)
//	{
//		return 1;
//	}
//	if (more == 1)
//	{
//		num = num - 1;
//	}
//	return num + Num2(num / 2 + more);
//}
//
//int main()
//{
//	int ret1 = Num1(20);
//	int ret2 = Num2(20);
//	printf("%d\n", ret1);
//	printf("%d\n", ret2);
//	return 0;
//}


//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };
//	char** cp[] = { c + 3,c + 2,c + 1,c };
//	char*** cpp = cp;
//	printf("%s\n", **++cpp);//POINT
//	printf("%s\n", *-- * ++cpp + 3);//ER - 加法优先级最低，++/--优先级最高
//	printf("%s\n", *cpp[-2] + 3);//ST - *cpp[-2]==* *(cpp-2)
//	printf("%s\n", cpp[-1][-1] + 1);//EW - cpp[-1][-1]==*(*(cpp-1)-1)
//	return 0;
//}


//int main()
//{
//	char* a[] = { "work","at","alibaba" };
//	char** pa = a;
//	pa++;
//	printf("%s\n", *pa);
//	return 0;
//}


//int main()
//{
//	int aa[2][5] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d\n", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}


//int  main()
//{
//    char c1, c2;
//    scanf("%c", &c1);
//    if (c1 >= 'A' && c1 <= 'Z' ) 
//    {
//        c2 = c1 + 32;
//        printf("%c", c2);
//    }
//    return 0;
//}


//int main()
//{
//	int a[5][5];
//	int(*p)[4];//p是一个数组指针，p指向的数组是4个整型元素的
//	p = a;//a的类型是int(*)[5] - p的类型是int(*)[4]
//	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);//|指针-指针|==指针与指针之间的元素个数
//	return 0;
//}


//int main()
//{
//	int a[3][2] = { (0,1),(2,3),(4,5) };// (  ， ) - 逗号表达式，从左到右表达最后一个式子
//	int* p;
//	p = a[0];							//a[0]是第一行数组名,表示第一行数组首元素地址，即a[0][0]
//	printf("%d\n", p[0]);
//	return 0;
//}


////在x86环境下
////假设结构体的大小是20个字节
//struct Text
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char cha[2];
//	short sBa[4];
//}*p=(struct Text*)0x100000;
//
//int main()
//{
//	printf("%p\n", p + 0x1);			   //0x100000+20==0x100014 - 16进制
//	printf("%p\n", (unsigned long)p + 0x1);//不是指针，为无符号长整型，+1就是+1
//	printf("%p\n", (unsigned int*)p + 0x1);//
//	return 0;
//}


//int main()
//{
//	int a[5] = { 1,2,3,4,5 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d\n", *(a + 1), *(ptr - 1));
//	return 0;
//}


//int main()
//{
//	int a[3][4] = { 0 };
//	printf("%zd\n", sizeof(a));//48 - 计算的是整个数组的大小，12*4，单位是字节
//	printf("%zd\n", sizeof(a[0][0]));//4 - 表示第一个元素，大小4个字节
//	printf("%zd\n", sizeof(a[0]));//16 - a[0]表示第一行数组，第一行四个元素，即4*4
//	printf("%zd\n", sizeof(a[0]+1));//4/8 - a[0]表示数组首元素地址，即a[0][0],+1后为a[0][1]的地址
//	printf("%zd\n", sizeof(*(a[0]+1)));//4 - 表示第一行数组第二个元素的地址，即a[0][1]
//	printf("%zd\n", sizeof(a+1));//4/8 - a为二维数组首元素地址，即为第一行地址， a+1表示数组第二行的地址
//	printf("%zd\n", sizeof(*(a+1)));//16 - 1.a+1是第二行地址，*(a+1)即第二行；2.*(a+1)==a[1]，为第二行数组名
//	printf("%zd\n", sizeof(&a[0]+1));//4/8 - &a[0]表示第一行数组的地址，+1后为第二行地址
//	printf("%zd\n", sizeof(*(&a[0] + 1)));//16 - &a[0]表示第一行数组的地址，+1后为第二行地址，*(&a[0] + 1)为第二行数组
//	printf("%zd\n", sizeof(*a));//16 - 1.a表示二维数组首元素地址，即第一行地址，解引用后为第一行；2.*a==*(a+0)==a[0]
//	printf("%zd\n", sizeof(a[3]));//16 - 二维数组“第四行”，无需真实存在，只需通过类型推断就可算出长度，计算的是第四行数组大小
//	return 0;
//}


//int main()
//{
//	const char* p = "abcdef";
//	printf("%zd\n", strlen(p));//6
//	printf("%zd\n", strlen(p+1));//5 - 从字符串第二个元素向后寻找\0
//	printf("%zd\n", strlen(&p));//随机值 - 表示p指针变量的地址，从指针变量起始位置向后寻找\0，与字符串无关
//	printf("%zd\n", strlen(&p+1));//随机值 - 表示p指针变量下一个的地址，与字符串无关
//	printf("%zd\n", strlen(&p[0]+1));//5 - &p[0]表示'a'的地址,&p[0]+1即第二个字符'b'的地址
//	printf("%zd\n", strlen(*p));//err - 'a' 97
//	printf("%zd\n", strlen(p[0]));//err - *(p+0)==*p - 'a' 97
//	return 0;
//}


//int main()
//{
//	const char* p = "abcdef";
//	printf("%zd\n", sizeof(p));//4/8 - p是指针变量
//	printf("%zd\n", sizeof(p+1));//4/8 - 表示的是b的地址
//	printf("%zd\n", sizeof(*p));//1 - p的类型是char*，*p就是char类型，大小为1个字节
//	printf("%zd\n", sizeof(p[0]));//1 - 1.p[0]==*(p+0)==*p=='a'；2.把常量字符串想象成数组，p可理解为数组名，p[0]就是首元素
//	printf("%zd\n", sizeof(&p));//4/8 - p的地址
//	printf("%zd\n", sizeof(&p+1));//4/8 - 表示的是跳过p指针变量的下一个的地址
//	printf("%zd\n", sizeof(&p[0] + 1));//4/8 - 表示的是字符串首字符地址的下一个，即第二个字符的地址
//	return 0;
//}


//int main()
//{
//	char arr[] = "abcdef";
//	printf("%zd\n", strlen(arr));//6 - 表示数组首元素的地址，
//	printf("%zd\n", strlen(arr+0));//6 - 表示数组首元素地址，从第一个元素向后寻找\0
//	printf("%zd\n", strlen(&arr));//6 - 表示数组的地址，但也是从数组第一个元素开始向后寻找\0 - &arr==char(*)[6]
//	printf("%zd\n", strlen(&arr+1));//随机值 - 表示跳过整个数组的下一个的地址
//	printf("%zd\n", strlen(&arr[0]+1));//5 - 表示数组中第一个元素的下一个，即第二个元素的地址
//	printf("%zd\n", strlen(*arr));//err - 表示数组首元素 - 'a' 97
//	printf("%zd\n", strlen(arr[1]));//err - 表示数组第二个元素 - 'b' 98
//	return 0;
//}


//int main()
//{
//	char arr[] = "abcdef";
//	printf("%zd\n", sizeof(arr));//7 - a b c d e f \0 - arr是数组名，计算的是数组总大小
//	printf("%zd\n", sizeof(arr+0));//4/8 - arr表示数组首元素的地址，arr+0也是
//	printf("%zd\n", sizeof(*arr));//1 - arr表示首元素地址，*arr即首元素
//	printf("%zd\n", sizeof(arr[1]));//1 - 表示数组第二个元素
//	printf("%zd\n", sizeof(&arr));//4/8 - &arr表示整个数组的地址
//	printf("%zd\n", sizeof(&arr+1));//4/8 - 表示跳过整个数组的下一个的地址
//	printf("%zd\n", sizeof(&arr[0]+1));//4/8 - &arr[0]表示首元素地址，&arr[0]+1即第二个元素地址
//	return 0;
//}


//int main()
//{
//	char arr[] = { 'a','b','c','d','e','f'};
//	printf("%zd\n", strlen(arr));//随机值 - arr是数组首元素地址，数组中没有\0，就会导致越界访问，产生随机结果
//	printf("%zd\n", strlen(arr+0));//随机值 - arr+0是数组首元素地址，数组中没有\0，就会导致越界访问，结果是随机的
//	printf("%zd\n", strlen(&arr));//随机值 - &arr是数组的地址，起始位置是数组的第一个元素
//	printf("%zd\n", strlen(&arr+1));//随机值 - 跳过整个数组的下一个地址
//	printf("%zd\n", strlen(&arr[0]+1));//随机值 - 跳过第一个元素的地址，即第二个元素的地址，从第二个元素开始向后统计
//	printf("%zd\n", strlen(*arr));//arr是首元素地址，*arr是首元素，即'a'，ASCII码值为97，相当于把97作为地址传递给strlen，得到的就是野指针不允许访问
//	printf("%zd\n", strlen(arr[1]));//arr[1]是第二个元素，即'b',ASCII码值为98，同上
//	return 0;
//}


//int main()
//{
//	char arr[] = { 'a','b','c','d','e','f'};
//	printf("%zd\n", sizeof(arr));//6 - 数组名单独放在sizeof内部，计算的是数组的大小，单位是字节
//	printf("%zd\n", sizeof(arr+0));//4/8 - 数组名表示首元素的地址。arr+0依然是首元素的地址
//	printf("%zd\n", sizeof(*arr));//4 - 数组名表示首元素的地址，*arr即首元素 - *arr==arr[0]==*(arr+0)
//	printf("%zd\n", sizeof(arr[1]));//1 - arr[1]是数组第二个元素
//	printf("%zd\n", sizeof(&arr));//4/8 - 数组的地址 - &arr==char(*)[6]
//	printf("%zd\n", sizeof(&arr+1));//4/8 - 跳过整个数组的地址
//	printf("%zd\n", sizeof(&arr[0]+1));//4/8 - 跳过第一个元素的地址，即第二个元素的地址
//	return 0;
//}


//int main()
//{
//		int a[] = { 1,2,3,4 };
//		printf("%zd\n", sizeof(a));//16 - sizeof(数组名) - 数组名表示整个数组
//		printf("%zd\n", sizeof(a + 0));//4/8 - a是首元素地址 - 类型是int*，a+0 还是首元素地址，大小是 4/8
//		printf("%zd\n", sizeof(*a));//4 - a是首元素地址，*a就是首元素，大小是4个字节 - *a==a[0]==*(a+0)
//		printf("%zd\n", sizeof(a + 1));//4/8 - a是首元素地址，类型是int*，a+1跳过一个整型，a+1是第二个元素的地址
//		printf("%zd\n", sizeof(a[1]));//4 - a[1]是第二个元素，大小4个字节
//		printf("%zd\n", sizeof(&a));//4/8 - &a是数组的地址
//		printf("%zd\n", sizeof(*&a));//16 - 1.*& 互相抵消了，sizeof(*&a)==sizeof(a); 2.&a是数组地址，类型是int(*)[4],对数组指针解引用访问的是数组，计算的是数组的大小
//		printf("%zd\n", sizeof(&a + 1));//4/8 - 跳过整个数组后的位置，依然是地址
//		printf("%zd\n", sizeof(&a[0]));//4/8 - 首元素的地址
//		printf("%zd\n", sizeof(&a[0] + 1));//4/8 - 跳过首元素地址的下一个，即数组第二个元素的地址
//		return 0;
//}


//sizeof - 操作符 - 计算变量所占内存空间大小 - 单位：字节 - 若操作数是类型，则计算使用类型创建的变量所占内存空间的大小 - 括号里表达式不参与计算
//strlen - 库函数 - 求字符串长度 - 统计的是函数参数的地址向后，\0 之前字符串中字符的个数

//int main()
//{
//	int a = 10;
//	printf("%zd\n", sizeof(a));
//	printf("%zd\n", sizeof a);      //证明sizeof不是函数
//	printf("%zd\n", sizeof(int));
//	int len = strlen("abcdef");		//" a b c d e f \0 "
//	printf("%d\n", len);
//	char arr[] = { 'a','b','c'};
//	printf("%zd\n", strlen(arr));	//随机值
//	return 0;
//}


//qsort - 库函数 - 直接可用来排序 - 底层使用的是快速排序的方式 - 可以排序任意类型数据
//void qsort( void* base,								//指针 - 指向待排序数组的第一个元素
//			size_t num,									//base指向的待排序数组的元素个数
//			size_t size,								//base指向的待排序数组的元素的大小
//			int (*compar)(const void*, const void*));   //函数指针 - 指向的就是两个元素的比较函数

//void Print(int arr[], int sz)
//{
//	int i = 0;
//	for ( i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//int cmp_int(const void* p1, const void* p2)
//{
//	/*if (*(int*)p1 > *(int*)p2)
//		return 1;
//	else if (*(int*)p1 == *(int*)p2)
//		return 0;
//	else
//		return -1;*/
//	return *(int*)p1 - *(int*)p2;
//}
//void text1()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	Print(arr, sz);
//}
//
//struct Stu
//{
//	char name[20];
//	int age;
//};
//void cmp_stu_by_name(const void* p1,const void* p2)
//{
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//	//strcmp按照对应字串中的字符的ASCII码值进行比较
//}
//void cmp_stu_by_age(const void* p1, const void* p2)
//{
//	return ((struct Stu*)p1)->age,-((struct Stu*)p2)->age;
//}
//void text2()
//{
//	struct Stu arr[3] = { {"C",18} ,{"B",19 }, {"A",20} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_stu_by_name);
//	qsort(arr, sz, sizeof(arr[0]), cmp_stu_by_age);
//}
//
//void Swap(char* buf1, char* buf2,size_t width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//void bubble_sort(void*base, size_t sz, size_t width,int (*cmp)(const void*p1,const void*p2))
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int j = 0;
//		int flag = 1;
//		for (j = 0; j < sz-1-i; j++)
//		{
//			if (cmp((char*)base+j*width,(char*)base+(j+1)*width)>0)
//			{
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width,width);
//				flag=0;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//}
//void text3()
//{
//	int arr1[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz1 = sizeof(arr1) / sizeof(arr1[0]);
//	bubble_sort(arr1, sz1, sizeof(arr1[0]), cmp_int);//排序整型
//	Print(arr1, sz1);
//}
//
//void text4()
//{
//	struct Stu arr[3] = { {"C",18} ,{"B",19 }, {"A",20} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz, sizeof(arr[0]), cmp_stu_by_name);
//	bubble_sort(arr, sz, sizeof(arr[0]), cmp_stu_by_age);//排序结构体
//}
//
//int main()
//{
//	//text1();//用qsort排序整型数据
//	//text2();//用qsort排序结构体的数据
//	//text3();
//	text4();
//	return 0;
//}


//void bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int j = 0;
//		int flag = 1;
//		for (j = 0; j < sz-1-i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = 0;
//				tmp = arr[j];
//				arr[j] = arr[j+1];
//				arr[j+1] = tmp;
//				flag=0;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//}
//void Print(int arr[], int sz)
//{
//	int i = 0;
//	for ( i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr)/sizeof(arr[0]);
//	bubble_sort(arr, sz);
//	Print(arr, sz);
//	return 0;
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void menu()
//{
//	printf("**************************\n");
//	printf("*********1.Add 2.Sub******\n");
//	printf("*********3.Mul 4.Div******\n");
//	printf("*********0.退出     ******\n");
//	printf("**************************\n");
//}
//
//void Calc(int(* pf)(int, int))
//{
//	int input;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	printf("输入两个数：");
//	scanf_s("%d%d", &x, &y);
//	ret = pf(x, y);
//	printf("%d\n", ret);
//}
//
//int main()
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择：");
//		scanf_s("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Calc(Add);
//			break;
//		case 2:
//			Calc(Sub);
//			break;
//		case 3:
//			Calc(Mul);
//			break;
//		case 4:
//			Calc(Div);
//			break;
//		case 0:
//			printf("退出");
//			break;
//		default:
//			printf("错误\n");
//			break;
//		}
//		printf("\n");
//		Sleep(1000);
//		system("cls");
//	} while (input);
//
//	return 0;
//}


//int main()
//{
//	int* (*pfArr[4])(int, int) = { 1,2,3,4, };
//	int* (*(*p)[4])(int, int) = &pfArr;//指向函数指针数组的指针
//	//取出的是函数指针数组的地址
//	//p是一个指向函数指针数组的指针
//	return 0;
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void menu()
//{
//	printf("**************************\n");
//	printf("*********1.Add 2.Sub******\n");
//	printf("*********3.Mul 4.Div******\n");
//	printf("*********0.退出     ******\n");
//	printf("**************************\n");
//}
//
//int main()
//{
//	int input;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	int* (*pfArr[5])(int, int) = {NULL,Add,Sub,Mul,Div};
//	do
//	{
//		menu();
//		printf("请选择：");
//		scanf_s("%d", &input);
//
//		if (input >= 1 && input <= 4)
//		{
//			printf("输入两个数：");
//			scanf_s("%d%d", &x, &y);
//			ret = pfArr[input](x, y);
//			printf("%d\n", ret);
//		}
//		else if (input == 0)
//		{
//			printf("退出\n");
//			break;
//		}
//		else
//		{
//			printf("错误");
//		}
//		printf("\n");
//		Sleep(1000);
//		system("cls");
//	} while (input);
//
//	return 0;
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//int main()
//{
//	int (*pfArr[4])(int, int) = { Add,Sub,Mul,Div };
//	int i = 0;
//	int x = 0;
//	int y = 0;
//	scanf("%d%d", &x, &y);
//	for (i = 0;i < 4; i++)
//	{
//		int ret = pfArr[i](x, y);
//		printf("%d\n", ret);
//	}
//	return 0;
//}


//typedef int* ptr_t;//重命名更加彻底
//#define PTR_T int*
//int main()
//{
//	ptr_t pa;//pa是整型指针
//	PTR_T pb;//pb是整型指针
//	ptr_t p1, p2;//p1，p2是整型指针
//	PTR_T p3, p4;//p3是指针，p4是整型
//	return 0;
//}


//typedef void(*pf_t)(int);
//void (*signal(int, void(*)(int)))(int);
//pf_t signal2(int, pf_t);


//void test(char*s)
//{
//}
//typedef void(*pf_t)(char*);//函数指针类型重命名
//int main()
//{
//	void(*pf)(char*) = test;
//	pf_t pf2 = test;
//	return 0;
//}


//typedef int(*parr_t)[5];//parr_t等价于int(*)[5]     //对数组指针重命名
//int main()
//{
//	int arr[5] = { 0 };
//	int(*p)[5] = &arr;//p是数组指针变量，p是变量的名字,int(*)[5]是数组指针类型
//	parr_t p2 = &arr;
//	return 0;
//}


//typedef int* pint;//typedef对指针类型重命名
//int main()
//{
//	int* p1 = NULL;
//	pint p2 = NULL;
//	return 0;
//}


//typedef unsigned int uint;//typedef - 用于类型重命名，可以将复杂的类型简化
//int main()
//{
//	unsigned int num1;
//	uint num2;
//	return 0;
//}


//void (*  signal(int, void(*)(int))  )(int);//函数声明
//声明的函数的名字：signal
//signal函数有两个参数，第一个参数类型是int，
//第二个参数的类型是void(*)(int)的函数指针类型，该指针可以指向一个函数，指向的函数参数是int，返回类型是void
//signal函数的返回类型是void(*)(int)的函数指针类型，该指针可以指向一个函数，指向的函数参数是int，返回类型是void


//int main()
//{
//	(*( void(*)() )0)();//函数调用
//	//1.将0强制类型转换为 void(*)() 类型的函数指针,参数为无参
//	//2.解引用，调用0地址处放的函数
//	return 0;
//}


//int Add(int x, int y)
//{
//	return(x + y);
//}
//
//int main()
//{
//	int x = 10;
//	int y = 20;
//	int z =Add(x, y);
//	printf("%d\n", z);
//	printf("%p\n", &Add);
//	printf("%p\n", Add);	     // &函数名 和 函数名 都表示函数的地址
//	int (*pf)(int,int) = &Add;	 //pf就是函数指针变量
//	int c = (*pf)(x, y);    //int c = pf(x, y); 
//	printf("%d\n", c);
//	return 0;
//}


//void test(int (*p)[5], int r, int c)
//{
//	int i = 0;
//	for (i = 0; i < r; i++)
//	{
//		int j = 0;
//		for (j = 0; j < c; j++)
//		{
//			printf("%d ",(*(p+i))[j]);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };//二维数组的首元素是第一行（一维数组），每一行是一个元素
//	test(arr,3,5);											//二维数组的数组名是第一行（一维数组）的地址
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int(*p)[10] = &arr;
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[10]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", (*p)[i]);
//	}
//	return 0;
//}


////数组指针是一种指针变量，是存放数组地址的指针变量
//int main()
//{
//	int arr1[10] = { 0 };
//	int(*p)[10] = &arr1;			//取出的是数组的地址 - p是数组指针
//	char arr2[5];
//	char(*p2)[5] = &arr2;
//	char* arr3[5];
//	char* (*p3)[5] = &arr3;
//	return 0;
//}


//int main()
//{
//	char str1[] = "hello";
//	char str2[] = "hello";
//	const char* str3 = "hello";
//	const char* str4 = "hello";
//	if (str1 == str2)
//		printf("same\n");
//	else
//		printf("not same\n");		//创建两个数组，开辟两块不同的空间
//	if (str3 == str4)
//		printf("same\n");			//常量字符串不能被修改，因此内容相同的常量字符串没必要保存两份
//	else
//		printf("not same\n");
//	return 0;
//}


//int main()
//{
//	const char* p = "Hello world!";  //p所指向的常量字符串不允许被修改
//	printf("%c\n", *p);				 //p存放首元素地址
//	printf("%s\n", p);
//	int len = strlen(p);
//	int i = 0;
//	for (i = 0; i < len; i++)
//	{
//		printf("%c", *(p+i));
//	}
//	return 0;
//}


////使用指针数组模拟实现二维数组
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 2,3,4,5,6 };
//	int arr3[] = { 3,4,5,6,7 };
//	int* arr[3] = { arr1,arr2,arr3 };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int* pa = &a;   //pa是指针变量，一级指针变量
//	int** ppa = &pa;//ppa是二级指针变量
//	                //二级指针变量是用来存放一级指针变量的地址
//	**ppa = 20;
//	printf("%d\n", a);
//	return 0;
//}


////冒泡排序：1.两两相邻的元素进行比较，不满足顺序就交换，满足就找下一对
//void bubble_sort(int arr[],int sz)
//{
//	int i = 0;
//	//趟数
//	for (i = 0; i < sz-1; i++)
//	{
//		//一趟冒泡排序
//		int flag = 1;//假设是有序的
//
//		int j = 0;
//		for (j = 0; j < sz-1-i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//				flag = 0;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//}
//
//void print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr,sz);
//	print_arr(arr, sz);
//	return 0;
//}

//1.一维数组传参时，传过去的是数组首元素的地址
//2.形参部分可以写成指针形式，也可以写成数组形式，但本质上都是指针，写成数组形式是为了方便理解

//void test(int arr[])//int* arr - 本质上arr是指针
//{
//	int sz2 = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", sz2);
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int sz1 = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", sz1);
//	test(arr);                              //这时arr没有单独放在sizeof内部，也没有&，所以arr是数组首元素的的地址
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	int* p = &arr[0];//int* p=arr;
//	for (i = 0; i < sz; i++)
//	{
//		scanf("%d", p + i);
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr)/sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	printf("arr       =%p\n", arr);
//	printf("arr+1     =%p\n", arr+1);
//
//	printf("&arr[0]   =%p\n", &arr[0]);       //数组名是数组首元素地址
//	printf("&arr[0]+1 =%p\n", &arr[0]+1);
//
//	printf("&arr	  =%p\n", &arr);		   //&arr - 数组的地址
//	printf("&arr+1    =%p\n", &arr+1);
//
//	printf("%d\n", sizeof(arr));   //例外：1.sizeof(数组名)：此时数组名表示整个数组，计算整个数组的大小，单位是字节
//								   //      2.&数组名：此时也表示整个数组，计算的是整个数组的地址
//	return 0;
//}


//void Swap(int* px, int* py)
//{
//	int z = 0;
//	z = *px;
//	*px = *py;
//	*py = z;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	Swap(&a, &b);
//	printf("%d %d\n", a, b);
//	return 0;
//}


//size_t my_strlen(const char* s)
//{
//	size_t count = 0;
//	assert(s != NULL);
//	while (*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int l = strlen(arr);//strlen - 统计字符串中 \0 之前字符的个数
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}


//int main()
//{
//	int a = 0;
//	int* pa = &a;
//	assert(pa != NULL);//assert - 断言 - 运行时确保程序符合指定条件，若不符合，就报错终止运行 
//	printf("%d\n", a);
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = &arr[0];
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p++) = i;
//	}
//	//此时p已经越界，可以把p置为NULL
//	p = NULL;
//	//下次使用时，判断p不为NULL的时候再使用
//	//...
//	p = &arr[0];
//	if (p != NULL)
//	{
//		//...
//	}
//	return 0;
//}


//int main()
//{
//	int a = 0;
//	int* pa = &a;
//	if (pa != NULL)		//1.使用之前检查有效性；2.指针变量不再使用时，及时置NULL
//	{
//		*pa = 20;
//	}
//	printf("%d\n", a);
//	return 0;
//}


//int main()
//{
//						//int* p; - err - 必须对指针进行初始化
//						//int* p; - 不初始化就是随机值 - 野指针
//	int* p = NULL;      //不是野指针 - NULL - 为0 - 地址无法访问
//	                    //*p = 20; - err - 无法进行解引用操作
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* p = &arr[0];
//	while (p < arr + sz)
//	{
//		printf("%d ",*p);
//		p++;
//	}
//	return 0;
//}


//int my_strlen(char* p)
//{
//	char* p1 = p;
//	while (*p != '\0')
//	{	
//		p++;
//	}
//	return p - p1;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}


//int my_strlen(char* p)          //arr=&arr[0]
//{
//	int count = 0;
//	while(*p != '\0')
//	{
//		count++;
//		p++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* p = &arr[0];
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* p = &arr[0];
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *p);
//		p++;
//	}
//	return 0;
//}

////const修饰指针变量：1.const放在*左边:限制的是*p，不能通过p改变p指向的对象，但可以指向其他对象
////                   2.const放在*右边：限制的是p，不能改变p的值，但p指向的内容可以修改
//int main()
//{
//	const int n = 10;
//	int m = 100;
//	const int* pa = &n;//*pa=20报错，但p=&m可行
//	int* const pb = &n;//*p=20可行，但p=&m报错
//
//	return 0;
//}

//int main()
//{
//	const int a = 0;    //const修饰变量 - const是常属性 - a不能被修改，但不是常量，是常变量 - c++中是常量
//	int* pa = &a;
//	*pa = 20;
//	printf("%d\n", a);
//	return 0;
//}

//int main()
//{
//	int a = 20;
//	printf("%p\n", &a);
//	int* pa = &a;         //地址 - 指针
//	printf("%p\n", pa);   //p是指针变量 - 存放指针（地址）的变量 - 指针变量是用来存放地址的
//	*pa = 25;              //解引用操作（间接访问操作）*p=a      //* - 解引用操作符
//	printf("%d\n", a);
//	return 0;
//}