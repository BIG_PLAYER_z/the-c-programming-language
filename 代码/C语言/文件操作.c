﻿#include"源.h"

//int main()
//{
//	FILE* pf = fopen("test.txt", "w");
//	fputs("abcdef", pf);//先将代码放在输出缓冲区
//	printf("睡眠10秒-已经写数据了，打开test.txt⽂件，发现⽂件没有内容\n");
//	Sleep(10000);
//	printf("刷新缓冲区\n");
//	fflush(pf);//刷新缓冲区时，才将输出缓冲区的数据写到⽂件（磁盘）
//	//注：fflush 在⾼版本的VS上不能使⽤了
//	printf("再睡眠10秒-此时，再次打开test.txt⽂件，⽂件有内容了\n");
//	Sleep(10000);
//	fclose(pf);
//	//注：fclose在关闭⽂件的时候，也会刷新缓冲区
//	pf = NULL;
//	return 0;
//}


////打开⽂件
//FILE* fopen(const char* filename, const char* mode);
//关闭⽂件
//int fclose(FILE* stream); 
// 
//fputc - int fputc ( int character, FILE * stream ); - 字符输出函数
//fgetc - int fgetc ( FILE * stream ); - 字符输入函数
//fputs - int fputs ( const char * str, FILE * stream ); - ⽂本⾏输出函数
//fgets - char * fgets ( char * str, int num, FILE * stream ); - ⽂本⾏输入函数
//fscanf - int fscanf ( FILE * stream, const char * format, ... ); - 格式化输⼊函数
//fprintf - int fprintf ( FILE * stream, const char * format, ... ); - 格式化输出函数
//fread - size_t fread ( void * ptr, size_t size, size_t count, FILE * stream ); - ⼆进制输⼊
//fwrite - size_t fwrite ( const void * ptr, size_t size, size_t count, FILE * stream ); - 二进制输出
//sprintf - int sprintf ( char * str, const char * format, ... ); - 把格式化的数据转化成字符串
//sscanf - int sscanf ( const char * s, const char * format, ...); - 在字符串中读取格式化的数据
//fseek - int fseek ( FILE * stream, long int offset, int origin ); - 根据⽂件指针的位置和偏移量来定位⽂件指针（⽂件内容的光标）
//ftell - long int ftell ( FILE * stream ); - 返回⽂件指针相对于起始位置的偏移量
//rewind - void rewind ( FILE * stream ); - 让⽂件指针的位置回到⽂件的起始位置
//feof - int feof ( FILE * stream ); - 当⽂件读取结束的时候，判断是读取结束的原因是否是：遇到⽂件尾结束 - 在⽂件读取过程中，不能⽤feof函数的返回值直接来判断⽂件的是否结束

//1. ⽂本⽂件读取是否结束，判断返回值是否为 EOF （ fgetc ），或者 NULL （ fgets ）
//例如：1.fgetc 判断是否为 EOF ;2. fgets 判断返回值是否为 NULL .

//⼆进制⽂件的读取结束判断，判断返回值是否⼩于实际要读的个数。
//例如： fread判断返回值是否⼩于实际要读的个数。


//int main()
//{
//	FILE*pf=fopen("text.txt","r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读取
//	int ch = 0;
//	while ((ch = fgetc(pf)) != EOF)
//	{
//		printf("%c\n", ch);
//	}
//	//判断什么原因导致读取结束
//	if (feof(pf))
//	{
//		printf("遇到文件末尾，读取正常结束\n");
//	}
//	else if (ferror(pf))
//	{
//		perror("fgetc");
//	}	
//	return 0;
//}


//int main()
//{
//	FILE* pf = fopen("text.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//	fseek(pf, -4, SEEK_END);
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	rewind(pf);
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//int main()
//{
//	FILE* pf = fopen("text.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//	//fseek(pf, 4, SEEK_CUR);
//	//fseek(pf,5,SEEK_SET);
//	fseek(pf,0,SEEK_END);
//	printf("%d\n", ftell(pf));
//	//ch = fgetc(pf);
//	//printf("%c\n", ch);//f
//
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//int main()
//{
//	int arr[5] = { 0 };
//	FILE* pf = fopen("text.txt", "rb");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写数据
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//fwrite(arr, sizeof(arr[0]), sz, pf);//以二进制形式写进去
//
//	//读取数据
//	int i = 0;
//	while (fread(arr + i, sizeof(int), 1, pf))
//	{
//		printf("%d ", arr[i]);
//		i++;
//	}
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5 };
//	FILE* pf = fopen("text.txt", "wb");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写数据
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//fwrite(arr, sizeof(arr[0]), sz, pf);//以二进制形式写进去
//
//	//读取数据
//	fread(arr, sizeof(arr[0]), sz, pf);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ",  arr[i]);
//	}
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//struct S
//{
//	char name[20];
//	int age;
//	float score;
//};
//int main()
//{
//	char buf[200] = { 0 };
//	struct S s = { "wzz",20,65.5f };
//	sprintf(buf, "%s %d %f", s.name, s.age, s.score);
//
//	printf("1以字符串形式:%s\n", buf);
//	struct S t = { 0 };
//	sscanf(buf, "%s %d %f", t.name, &(t.age), &(t.score));
//	printf("2按照格式打印:%s %d %f\n", t.name, t.age, t.score);
//	return 0;
//}


//struct S
//{
//	char name[20];
//	int age;
//	float score;
//};
//
//int main()
//{
//	struct S s = { "wzz",20,65.5f };
//	//把s中的数据放在文件里
//	FILE*pf=fopen("text.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	fscanf(pf, "%s %d %f", s.name, &(s.age), &(s.score));
//	//写文件 - 以文本的形式写入
//	fprintf(pf, "%s %d %f", s.name, s.age, s.score);
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//int main()
//{
//	FILE* pf = fopen("text.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写文件
//	fputs("Hello World\n", pf);
//	fputs("Hello World\n", pf);
//	//读文件
//	char arr[20] = { 0 };
//	while (fgets(arr, 20, pf) != NULL)
//	{
//		printf("%s", arr);
//	}
//	//fgets(arr,20, pf);          //读取包括'\0'，因此实际读取字符个数仅为num-1 - 若长度允许，则读取'\n'后再读取'\0'
//	//printf("%s\n", arr);
//	
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//int main()
//{
//	FILE*pf=fopen("text.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	//int c=fgetc(pf);//1.如果读取成功，返回读取字符的ASCII值；2.如果读取失败，返回EOF
//	//printf("%c ", c);
//	//c = fgetc(pf);
//	//printf("%c ", c);
//	int c = 0;
//	while ((c = fgetc(pf)) != EOF)
//	{
//		printf("%c ", c);
//	}
//	//写文件
//	/*fputc('Z', pf);
//	char ch = 0;
//	for (ch = 'a'; ch <= 'z'; ch++)
//	{
//		fputc(ch, pf);
//	}*/
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//⽂件指针
//缓冲⽂件系统中，关键的概念是“⽂件类型指针”，简称“⽂件指针”。
//每个被使⽤的⽂件都在内存中开辟了⼀个相应的⽂件信息区，⽤来存放⽂件的相关信息（如⽂件的名字，
// ⽂件状态及⽂件当前的位置等）。这些信息是保存在⼀个结构体变量中的。该结构体类型是由系统声明的，取名 FILE.


//int main()
//{
//	int a = 10000;
//	FILE* pf = fopen("test.txt", "wb");
//	fwrite(&a, 4, 1, pf);//⼆进制的形式写到⽂件中
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}