﻿#include"源.h"


//枚举顾名思义就是⼀⼀列举。把可能的取值⼀⼀列举。
//{}中的内容是枚举类型的可能取值，也叫 枚举常量。
//这些可能取值都是有值的，默认从0开始，依次递增1，当然在声明枚举类型的时候也可以赋初值。

//枚举的优点：
//1. 增加代码的可读性和可维护性
//2. 和#define定义的标识符⽐较枚举有类型检查，更加严谨。
//3. 便于调试，预处理阶段会删除 #define 定义的符号
//4. 使⽤⽅便，⼀次可以定义多个常量
//5. 枚举常量是遵循作⽤域规则的，枚举声明在函数内，只能在函数内使⽤

//enum Option
//{
//	EXIT,
//	ADD,
//	SUB,
//	MUL,
//	DIV
//};
//
//int main()
//{
//	int input = 0;
//	printf("请选择：");
//	scanf("%d", &input);
//	switch (input)
//	{
//	case ADD:
//
//		break;
//	case SUB:
//
//		break;
//	case MUL:
//
//		break;
//	case DIV:
//
//		break;
//	case EXIT:
//
//		break;
//	default:
//		printf("输入错误\n");
//		break;
//	}
//	return 0;
//}


//enum Day//星期
//{
//	Mon,
//	Tues,
//	Wed,
//	Thur,
//	Fri,
//	Sat,
//	Sun
//};
//
//enum Sex//性别
//{
//	MALE,
//	FEMALE,
//	SECRET
//};
//
//enum Color//颜⾊
//{
//	RED = 2,
//	GREEN = 4,
//	BLUE = 8
//};
//
//int main()
//{
//	enum Color co = RED;
//	printf("%d\n", MALE);
//	printf("%d\n", FEMALE);
//	printf("%d\n", SECRET);
//	return 0;
//}


//判断大小端存储
//int check_sys()
//{
//	union
//	{
//		int i;
//		char c;
//	}un;
//	un.i = 1;
//	return un.c;
//}
//
//int main()
//{
//	int ret = check_sys();
//	if (ret == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}


//struct gift_list
//{
//	int stock_number;//库存量
//	double price; //定价
//	int item_type;//商品类型
//	union {
//		struct Book
//		{
//			char title[20];//书名
//			char author[20];//作者
//			int num_pages;//⻚数
//		}book;
//		struct Mug
//		{
//			char design[30];//设计
//		}mug;
//		struct Shirt
//		{
//			char design[30];//设计
//			int colors;//颜⾊
//			int sizes;//尺⼨
//		}shirt;
//	}item;
//};
//
//int main()
//{
//	struct gift_list gl;
//	return 0;
//}


//编译器只为最⼤的成员分配⾜够的内存空间。联合体的特点是所有成员共⽤同⼀块内存空间。所以联合体也叫：共⽤体。
//给联合体其中⼀个成员赋值，其他成员的值也跟着变化。
//联合的⼤⼩⾄少是最⼤成员的⼤⼩。当最⼤成员⼤⼩不是最⼤对⻬数的整数倍的时候，就要对⻬到最⼤对⻬数的整数倍。

//union Un
//{
//	char c;
//	int i;
//};
//
//int main()
//{
//	//联合变量的定义
//	union Un un = { 0 };
//	//计算连个变量的⼤⼩
//	printf("%zd\n", sizeof(un));
//	printf("%p\n", &(un.i));
//	printf("%p\n", &(un.c));
//	printf("%p\n", &un);
//	un.i = 0x11223344;
//	un.c = 0x55;
//	printf("%x\n", un.i);
//	return 0;
//}