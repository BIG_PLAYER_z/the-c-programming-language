﻿#include"源.h"


//条件编译 - 在编译⼀个程序的时候我们如果要将⼀条语句（⼀组语句）编译或者放弃是很⽅便的。因为我们有条件编译指令。

//#define MAX 1
//int main()
//{
//#if defined(MAX)
//	printf("ok\n");
//#endif
//#ifdef MAX
//	printf("ok\n");
//#endif
//#if !defined(MAX)
//	printf("NO\n");
//#endif
//#ifndef MAX
//	printf("NO\n");
//#endif
//	return 0;
//}


//#define M 1
//int main()
//{
//#if M==0
//	printf("0\n");
//#elif M==1
//	printf("1\n");
//#elif M==2
//	printf("2\n");
//#else
//	printf("!\n")
//#endif
//	return 0;
//}


//1.
//#if 常量表达式
////...
//#endif
////常量表达式由预处理器求值。
//如：
//#define __DEBUG__ 1
//#if __DEBUG__
////..
//#endif
//2.多个分⽀的条件编译
//#if 常量表达式
////...
//#elif 常量表达式
////...
//#else
////...
//#endif
//3.判断是否被定义
//#if defined(symbol)
//#ifdef symbol
//
//#if !defined(symbol)
//#ifndef symbol
//4.嵌套指令
//#if defined(OS_UNIX)
//			 #ifdef OPTION1
//					unix_version_option1();
//		#endif
//		#ifdef OPTION2
//			unix_version_option2();
//		#endif
//#elif defined(OS_MSDOS)
//		#ifdef OPTION2
//			msdos_version_option2();
//		#endif
//#endif


//#define M 10
//int main()
//{
//#if M>0
//	printf("haha\n");
//#endif
//	return 0;
//}


//#define __DEBUG__
//int main()
//{
//	int i = 0;
//	int arr[10] = { 0 };
//	for (i = 0; i < 10; i++)
//	{
//		arr[i] = i;
//		#ifdef __DEBUG__
//		printf("%d\n", arr[i]);//为了观察数组是否赋值成功。 
//		#endif //__DEBUG__
//	}
//	return 0;
//}


//命令⾏定义 - 许多C 的编译器提供了⼀种能⼒，允许在命令⾏中定义符号。⽤于启动编译过程。


// #undef - ⽤于移除⼀个宏定义 
//#undef NAME
//如果现存的⼀个名字需要被重新定义，那么它的旧名字⾸先要被移除。


//#运算符将宏的⼀个参数转换为字符串字⾯量。它仅允许出现在带参数的宏的替换列表中。
//#运算符所执⾏的操作可以理解为”字符串化“。
// 
//## 可以把位于它两边的符号合成⼀个符号，它允许宏定义从分离的⽂本⽚段创建标识符。 ## 被称为记号粘合
//这样的连接必须产⽣⼀个合法的标识符。否则其结果就是未定义的

//int int_max(int x, int y)
//{
//	return x > y ? x : y;
//}
//float float_max(float x, float y)
//{
//	return x > y ? x : y;
//}
//宏定义
//#define GENERIC_MAX(type) \
//		type type##_max(type x, type y)\
//		{ \
//		return (x>y?x:y); \
//		}
////定义函数
//GENERIC_MAX(int);
//GENERIC_MAX(float);
//int main()
//{
//	int r1 = int_max(3, 5);
//	printf("%d\n", r1);
//	float r2 = float_max(3.1f, 4.5f);
//	printf("%f\n", r2);
//	return 0;
//}


//#define Print(n,format) printf("the value of "#n" is "format"\n",n)
//int main()
//{
//	int a = 1;
//	Print(a, "%d");
//	int b = 20;
//	Print(b, "%d");
//	float f = 3.14;
//	Print(f, "%f");
//	return 0;
//}


//1. ⽤于调⽤函数和从函数返回的代码可能⽐实际执⾏这个⼩型计算⼯作所需要的时间更多。所以宏⽐函数在程序的规模和速度⽅⾯更胜⼀筹。
//2. 更为重要的是函数的参数必须声明为特定的类型。所以函数只能在类型合适的表达式上使⽤。反之
//这个宏怎可以适⽤于整形、⻓整型、浮点型等可以⽤于 > 来⽐较的类型。宏的参数是类型⽆关的。
//和函数相⽐宏的劣势：
//1. 每次使⽤宏的时候，⼀份宏定义的代码将插⼊到程序中。除⾮宏⽐较短，否则可能⼤幅度增加程序的⻓度。
//2. 宏是没法调试的。
//3. 宏由于类型⽆关，也就不够严谨。
//4. 宏可能会带来运算符优先级的问题，导致程容易出现错

//#define MALLOC(num, type)\
// (type )malloc(num sizeof(type))
////使⽤
//MALLOC(10, int);//类型作为参数
////预处理器替换之后：
//(int*)malloc(10 sizeof(int));


//宏替换的规则
//在程序中扩展#define定义符号和宏时，需要涉及?个步骤。
//1. 在调?宏时，?先对参数进?检查，看看是否包含任何由#define定义的符号。如果是，它们?先被替换。
//2. 替换?本随后被插?到程序中原来?本的位置。对于宏，参数名被他们的值所替换。
//3. 最后，再次对结果?件进?扫描，看看它是否包含任何由#define定义的符号。如果是，就重复上述处理过程。
//注意：
//1. 宏参数和#define 定义中可以出现其他#define定义的符号。但是对于宏，不能出现递归。
//2. 当预处理器搜索#define定义的符号的时候，字符串常量的内容并不被搜索。


//当宏参数在宏的定义中出现超过?次的时候，如果参数带有副作?，那么你在使?这个宏的时候就可能出现危险，导致不可预测的后果。副作?就是表达式求值的时候出现的永久性效果。
//#define MAX(X,Y)((X)>(Y)?(X):(Y))
//int main()
//{
//	int a = 3;
//	int b = 5;
//	int c = MAX(a++, b++);          //x++ - 带有副作用
//	printf("a=%d\n", a);
//	printf("b=%d\n", b);
//	printf("c=%d\n", c);
//	return 0;
//}


//#define SQUARE(x) ((x)*(x))
//int main()
//{
//	int a = 5;
//	int ret = SQUARE(a);
//	printf("%d\n", ret);
//	return 0;
//}


//int main()
//{
//	printf("%s\n", __FILE__); //进?编译的源?件
//	printf("%d\n", __LINE__);//?件当前的?号
//	printf("%s\n", __DATE__); //?件被编译的?期
//	printf("%s\n", __TIME__); //?件被编译的时间
//	//printf("%d\n", __STDC__); //如果编译器遵循ANSI C，其值为1，否则未定义
//	return 0;
//}