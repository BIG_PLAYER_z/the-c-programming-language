﻿#include"snake.h"


void SetPos(short x, short y)
{
	//获得标准输出设备的句柄
	HANDLE houtput = NULL;
	houtput = GetStdHandle(STD_OUTPUT_HANDLE);
	
	//定位光标的位置
	COORD pos = { x,y };
	SetConsoleCursorPosition(houtput, pos);
}

void WelcomeToGame()
{
	SetPos(44, 12);
	wprintf(L"贪吃蛇\n");
	SetPos(40, 13);
	wprintf(L"制作人：王铮喆\n");
	SetPos(42, 20);
	system("pause");
	system("cls");
	SetPos(0, 10);
	wprintf(L"玩法介绍：用↑↓←→来控制蛇的移动，不能穿墙，不能咬到自己，F3加速、F4减速，ESC退出、SPACE暂停\n");
	SetPos(0, 11);
	wprintf(L"分数介绍：一颗果子初始分值10分，加速一次分值+2，减速一次分值-2，最高分值18，最低分值2\n");

	SetPos(42, 20);
	system("pause");
	system("cls");
}


void CreatMap()
{
	//上
	for(int i=0;i<29;i++)
	{
		wprintf(L"%lc",WALL);
	}
	//下
	SetPos(0, 26);
	for (int i = 0; i < 29; i++)
	{
		wprintf(L"%lc", WALL);
	}
	//左
	for (int i = 1; i <=25; i++)
	{
		SetPos(0, i);
		wprintf(L"%lc",WALL);
	}
	//右
	for (int i = 1; i <= 25; i++)
	{
		SetPos(56, i);
		wprintf(L"%lc",WALL);
	}
}


void InitSnake(pSnake ps)
{
	pSnakeNode cur = NULL;
	for (int i = 0; i < 3; i++)
	{
		cur = (pSnakeNode)malloc(sizeof(SnakeNode));
		if (cur == NULL)
		{
			perror("InitSnake()::malloc()");
			return;
		}
		cur->next = NULL;
		cur->x = POS_X + 2 * i;
		cur->y = POS_Y;

		//头插法插入链表
		if (ps->_pSnake == NULL)
		{
			ps->_pSnake = cur;
		}
		else
		{
			cur->next = ps->_pSnake;
			ps->_pSnake = cur;
		}
	}

	cur = ps->_pSnake;
	while (cur)
	{
		if (cur == ps->_pSnake)
		{
			SetPos(cur->x, cur->y);
			wprintf(L"%lc", HEAD);
		}
		else
		{
			SetPos(cur->x, cur->y);
			wprintf(L"%lc", BODY);
		}
		cur = cur->next;
	}

	//设置贪吃蛇属性
	ps->_dir = RIGHT;//默认向右
	ps->_score = 0;
	ps->_food_weight = 10;
	ps->_sleep_time = 200;
	ps->_status = OK;
}


void CreatFood(pSnake ps)
{
	int x = 0;
	int y = 0;

	//生成x是2的倍数
	//x:2-54
	//y:1-25
again:
	do
	{
		x = rand() % 53 + 2;
		y = rand() % 25 + 1;
	} while (x % 2 != 0);

	//x和y的坐标不能和蛇的身体坐标冲突
	pSnakeNode cur = ps->_pSnake;
	while (cur)
	{
		if (x == cur->x && y == cur->y)
		{
			goto again;
		}
		cur = cur->next;
	}

	//创建食物节点
	pSnakeNode pFood = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pFood == NULL)
	{
		perror("CreatFood()::malloc()");
		return;
	}

	pFood->x = x;
	pFood->y = y;
	pFood->next = NULL;

	SetPos(x, y);
	wprintf(L"%lc", FOOD);

	ps->_pFood = pFood;
}


//游戏的初始化
void GameStart(pSnake ps)
{
	//0.设置窗口大小
	system("mode con cols=100 lines=30");
	system("title 贪吃蛇");

	//1.隐藏光标
	HANDLE houtput = GetStdHandle(STD_OUTPUT_HANDLE);
	//影藏光标操作
	CONSOLE_CURSOR_INFO CursorInfo;
	GetConsoleCursorInfo(houtput, &CursorInfo);//获取控制台光标信息
	CursorInfo.bVisible = false; //隐藏控制台光标
	SetConsoleCursorInfo(houtput, &CursorInfo);//设置控制台光标状态

	//2.打印欢迎界面
	//3.玩法介绍
	WelcomeToGame();
	
	//4.绘制地图
	CreatMap();
	
	//5.创建蛇+设置游戏相关信息
	InitSnake(ps);
	
	//6.创建食物
	CreatFood(ps);
}


//打印帮助信息
void PrintHelpInfo()
{
	SetPos(64, 10);
	wprintf(L"%ls", L"不能穿墙，不能咬到自己");
	SetPos(64, 11);
	wprintf(L"%ls", L"用 ↑. ↓ . ← . → 来控制蛇的移动");
	SetPos(64, 12);
	wprintf(L"%ls", L"F3加速、F4减速");
	SetPos(64, 13);
	wprintf(L"%ls", L"ESC退出、SPACE暂停");
	SetPos(64, 20);
	wprintf(L"%ls", L"WZZ制作");
}

#define KEY_PRESS(VK) ( (GetAsyncKeyState(VK) & 1) ? 1 : 0 )

//游戏暂停
void Pause()
{
	while (1)
	{
		Sleep(200);
		if (KEY_PRESS(VK_SPACE))
		{
			break;
		}
	}
}

//判断下一坐标处是否是食物
int NextIsFood(pSnakeNode pn, pSnake ps)
{
	return (ps->_pFood->x == pn->x && ps->_pFood->y == pn->y);
}

//下一个位置是食物，吃掉食物
void EatFood(pSnakeNode pn, pSnake ps)
{
	//头插
	ps->_pFood->next = ps->_pSnake;
	ps->_pSnake = ps->_pFood;

	//释放下一个位置的节点
	free(pn);
	pn = NULL;

	pSnakeNode cur = ps->_pSnake;

	//打印蛇
	while (cur)
	{
		if (cur == ps->_pSnake)
		{
			SetPos(cur->x, cur->y);
			wprintf(L"%lc", HEAD);
		}
		else
		{
			SetPos(cur->x, cur->y);
			wprintf(L"%lc", BODY);
		}
		cur = cur->next;
	}
	ps->_score += ps->_food_weight;

	//重新创建食物
	CreatFood(ps);
}

//下一个位置不是食物
void NoFood(pSnakeNode pn, pSnake ps)
{
	//头插
	pn->next = ps->_pSnake;
	ps->_pSnake = pn;

	pSnakeNode cur = ps->_pSnake;
	while (cur->next->next!=NULL)
	{
		if (cur == ps->_pSnake)
		{
			SetPos(cur->x, cur->y);
			wprintf(L"%lc", HEAD);
		}
		else
		{
			SetPos(cur->x, cur->y);
			wprintf(L"%lc", BODY);
		}
		cur = cur->next;
	}

	//把最后一个节点打印为空格
	SetPos(cur->next->x, cur->next->y);
	printf("  ");

	//释放最后一个节点
	free(cur->next);
	//把倒数第二个节点的地址置为空
	cur->next = NULL;
}

//检测蛇是否撞墙
void KillByWall(pSnake ps)
{
	if (ps->_pSnake->x == 0 || ps->_pSnake->x == 56 ||ps->_pSnake->y == 0 || ps->_pSnake->y == 26)
	{
		ps->_status = KILL_BY_WALL;
	}
}

//检测蛇是否撞到自己
void KillBySelf(pSnake ps)
{
	pSnakeNode cur = ps->_pSnake->next;
	while (cur)
	{
		if(cur->x == ps->_pSnake->x && cur->y == ps->_pSnake->y)
		{
			ps->_status = KILL_BY_SELF;
			break;
		}
		cur = cur->next;
	}
}

//蛇的移动
void SnakeMove(pSnake ps)
{
	//创建一个节点，表示蛇即将到达的下一个节点
	pSnakeNode pNextNode = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pNextNode == NULL)
	{
		perror("pNextNode()::malloc()");
		return;
	}
	switch (ps->_dir)
	{
	case UP:
		pNextNode->x = ps->_pSnake->x;
		pNextNode->y = ps->_pSnake->y - 1;
		break;
	case DOWN:
		pNextNode->x = ps->_pSnake->x;
		pNextNode->y = ps->_pSnake->y + 1;
		break;
	case LEFT:
		pNextNode->x = ps->_pSnake->x - 2;
		pNextNode->y = ps->_pSnake->y;
		break;
	case RIGHT:
		pNextNode->x = ps->_pSnake->x + 2;
		pNextNode->y = ps->_pSnake->y;
		break;
	}

	//检测下一个坐标处是否是食物
	if (NextIsFood(pNextNode,ps))
	{
		EatFood(pNextNode,ps);
	}
	else
	{
		NoFood(pNextNode, ps);
	}

	//检测蛇是否撞墙
	KillByWall(ps);
	
	//检测蛇是否撞到自己
	KillBySelf(ps);
}

//游戏运行
void GameRun(pSnake ps)
{
	//打印帮助信息
	PrintHelpInfo();

	do
	{
		//打印总分和食物的分值
		SetPos(64, 8);
		wprintf(L"总分数：%ld", ps->_score);
		SetPos(64, 9);
		wprintf(L"食物分数：%2ld", ps->_food_weight);

		if (KEY_PRESS(VK_UP) && ps->_dir != DOWN)
		{
			ps->_dir = UP;
		}
		else if (KEY_PRESS(VK_DOWN) && ps->_dir != UP)
		{
			ps->_dir = DOWN;
		}
		else if (KEY_PRESS(VK_RIGHT) && ps->_dir != LEFT)
		{
			ps->_dir = RIGHT;
		}
		else if (KEY_PRESS(VK_LEFT) && ps->_dir != RIGHT)
		{
			ps->_dir = LEFT;
		}
		else if (KEY_PRESS(VK_SPACE))
		{
			//暂停
			Pause();
		}
		else if (KEY_PRESS(VK_ESCAPE))
		{
			//正常退出游戏
			ps->_status = END_NORMAL;
		}
		else if (KEY_PRESS(VK_F3))
		{
			//加速
			if(ps->_sleep_time>80)
			{
				ps->_sleep_time -= 30;
				ps->_food_weight += 2;
			}
		}
		else if (KEY_PRESS(VK_F4))
		{
			//减速
			if (ps->_food_weight > 2)
			{
				ps->_sleep_time += 30;
				ps->_food_weight -= 2;
			}
		}

		SnakeMove(ps);

		Sleep(ps->_sleep_time);

	} while (ps->_status==OK);
}

//结束游戏 - 善后工作
void GameEnd(pSnake ps)
{
	SetPos(24, 12);
	switch (ps->_status)
	{
	case END_NORMAL:
		printf("结束游戏\n");
		break;
	case KILL_BY_WALL:
		printf("撞墙，游戏失败\n");
		break;
	case KILL_BY_SELF:
		printf("撞到自己，游戏结束\n");
		break;
	}

	//释放蛇身链表
	pSnakeNode cur = ps->_pSnake;
	while (cur)
	{
		pSnakeNode del = cur;
		cur = cur->next;
		free(del);
	}
}
