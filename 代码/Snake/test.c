#include"snake.h"


//完成游戏的测试逻辑
void test01()
{
	int ch = 0;
	do
	{
		system("cls");

		//创建贪吃蛇
		Snake snake = { 0 };

		//初始化游戏
		GameStart(&snake);

		//运行游戏
		GameRun(&snake);

		//结束游戏 - 善后工作
		GameEnd(&snake);
		SetPos(20, 15);
		printf("是否再来一局？(Y/N)：");
		ch = getchar();
		while (getchar() != '\n');//清理'\n'
	} while (ch == 'Y' || ch == 'y');
	SetPos(0, 27);

}


int main()
{
	//设置适配本地环境
	setlocale(LC_ALL,"");
	srand((unsigned int)time(NULL));
	test01();
	return 0;
}