﻿//#include"snake.h"

//ststem 函数可以用来执行系统命令

//int main()
//{
//	//设置控制台的相关属性
//	system("mode con cols=100 lines=30");
//  //更改控制台名称
//	system("title 贪吃蛇");
//	//getchar();
//	system("pause");
//	return 0;
//}


//COORD - COORD 是Windows API中定义的⼀个结构体，表⽰⼀个字符在控制台屏幕幕缓冲区上的坐标，坐标系(0，0) 的原点位于缓冲区的顶部左侧单元格

//int main()
//{
//	COORD pos1 = { 0,0 };
//	COORD pos2 = { 10,20 };
//	return 0;
//}


//GetStdHandle -  是⼀个Windows API函数。它⽤于从⼀个特定的标准设备（标准输⼊、标准输出或标准错误）
//中取得⼀个句柄（⽤来标识不同设备的数值），使⽤这个句柄可以操作设备
//HANDLE GetStdHandle(DWORD nStdHandle);

//GetConsoleCursorInfo - 检索有关指定控制台屏幕缓冲区的光标⼤⼩和可⻅性的信息 
//BOOL WINAPI GetConsoleCursorInfo(HANDLE hConsoleOutput,PCONSOLE_CURSOR_INFO lpConsoleCursorInfo);
//PCONSOLE_CURSOR_INFO 是指向 CONSOLE_CURSOR_INFO 结构的指针，该结构接收有关主机游标（光标）的信息

//CONSOLE_CURSOR_INFO - 包含有关控制台光标的信息
//dwSize，由光标填充的字符单元格的百分⽐。 此值介于1到100之间。光标外观会变化，范围从完全填充单元格到单元底部的⽔平线条。
//bVisible，游标的可⻅性。 如果光标可⻅，则此成员为 TRUE。

//SetConsoleCursorInfo - 设置指定控制台屏幕缓冲区的光标的⼤⼩和可⻅性
//BOOL WINAPI SetConsoleCursorInfo(HANDLE hConsoleOutput,const CONSOLE_CURSOR_INFO* lpConsoleCursorInfo);

//SetConsoleCursorPosition - 设置指定控制台屏幕缓冲区中的光标位置，我们将想要设置的坐标信息放在COORD类型的pos中，调
//⽤SetConsoleCursorPosition函数将光标位置设置到指定的位置。
//BOOL WINAPI SetConsoleCursorPosition(HANDLE hConsoleOutput,COORD pos);

//int main()
//{
//	//获得标准输出设备的句柄
//	HANDLE houtput = GetStdHandle(STD_OUTPUT_HANDLE);
//
//	//定义一个光标信息的结构体
//	CONSOLE_CURSOR_INFO cursor_info = { 0 };
//
//	//获取和houtput句柄相关的控制台上的光标信息，存放在cursor_info中
//	GetConsoleCursorInfo(houtput, &cursor_info);
//
//	//修改光标的占比
//	//cursor_info.dwSize = 100;
//	cursor_info.bVisible = false;
//
//	//设置和houtput句柄相关的控制台上的光标信息
//	SetConsoleCursorInfo(houtput, &cursor_info);
//
//	system("pause");
//	return 0;
//}


//int main()
//{
//	//获得标准输出设备的句柄
//	HANDLE houtput = GetStdHandle(STD_OUTPUT_HANDLE);
//	
//	//定位光标的位置
//	COORD pos = { 10,20 };
//	SetConsoleCursorPosition(houtput, pos);
//
//	system("pause");
//	return 0;
//}


//void set_pos(short x,short y)
//{
//	//获得标准输出设备的句柄
//	HANDLE houtput = NULL;
//	houtput = GetStdHandle(STD_OUTPUT_HANDLE);
//	
//	//定位光标的位置
//	COORD pos = { x,y };
//	SetConsoleCursorPosition(houtput, pos);
//}
//
//int main()
//{
//	set_pos(10,20);
//	printf("666\n");
//
//	set_pos(10, 10);
//	printf("777");
//	getchar();
//	system("pasue");
//	return 0;
//}


//GetAsyncKeyState - 获取按键情况 - 将键盘上每个键的虚拟键值传递给函数，函数通过返回值来分辨按键的状态。
//SHORT GetAsyncKeyState(int vKey);
//GetAsyncKeyState 的返回值是short类型，在上⼀次调⽤ GetAsyncKeyState 函数后，如果返回的16位的short数据中，最⾼位是1，
//说明按键的状态是按下，如果最⾼是0，说明按键的状态是抬起；如果最低位被置为1则说明，该按键被按过，否则为0。
//如果我们要判断⼀个键是否被按过，可以检测GetAsyncKeyState返回值的最低值是否为1

//#define KEY_PRESS(VK) ( (GetAsyncKeyState(VK) & 0x1) ? 1 : 0 )
//int main()
//{
//	while (1)
//	{
//		if (KEY_PRESS(0x30))
//		{
//			printf("0\n");
//		}
//		else if (KEY_PRESS(0x31))
//		{
//			printf("1\n");
//		}
//		else if (KEY_PRESS(0x32))
//		{
//			printf("2\n");
//		}
//		else if (KEY_PRESS(0x33))
//		{
//			printf("3\n");
//		}
//		else if (KEY_PRESS(0x34))
//		{
//			printf("4\n");
//		}
//		else if (KEY_PRESS(0x35))
//		{
//			printf("5\n");
//		}
//		else if (KEY_PRESS(0x36))
//		{
//			printf("6\n");
//		}
//		else if (KEY_PRESS(0x37))
//		{
//			printf("7\n");
//		}
//		else if (KEY_PRESS(0x38))
//		{
//			printf("8\n");
//		}
//		else if (KEY_PRESS(0x39))
//		{
//			printf("9\n");
//		}
//	}
//	return 0;
//}


//setlocale - ⽤于修改当前地区，可以针对⼀个类项修改，也可以针对所有类项	
//char* setlocale (int category, const char* locale);
//setlocale 的第⼀个参数可以是前⾯说明的类项中的⼀个，那么每次只会影响⼀个类项，如果第⼀个参数是LC_ALL，就会影响所有的类项。
//C标准给第⼆个参数仅定义了2种可能取值："C"（正常模式）和" "（本地模式）。
//在任意程序执⾏开始，都会隐藏式执⾏调⽤ :setlocale(LC_ALL, "C");

//int main()
//{
//	char* ret = setlocale(LC_ALL, NULL);
//	printf("%s\n", ret);
//	ret = setlocale(LC_ALL, "");
//	printf("%s\n", ret);
//	return 0;
//}


//int main ()
//{
//  time_t rawtime;
//  struct tm * timeinfo;
//  char buffer [80];
//
//  struct lconv * lc;
//
//  time ( &rawtime );
//  timeinfo = localtime ( &rawtime );
//
//  int twice=0;
//
//  do {
//    printf ("Locale is: %s\n", setlocale(LC_ALL,NULL) );
//
//    strftime (buffer,80,"%c",timeinfo);
//    printf ("Date is: %s\n",buffer);
//
//    lc = localeconv ();
//    printf ("Currency symbol is: %s\n-\n",lc->currency_symbol);
//
//    setlocale (LC_ALL,"");
//  } while (!twice++);
//
//  return 0;
//}


//宽字符的字⾯量必须加上前缀“L”，否则 C 语⾔会把字⾯量当作窄字符类型处理。
//前缀“L”在单引号前⾯，表⽰宽字符，对应 wprintf() 的占位符为% lc ；在双引号前⾯，表⽰宽字符串，对应wprintf() 的占位符为% ls

//int main()
//{
//	//设置本地化
//	setlocale(LC_ALL, "");
//
//	char a = 'a';
//	char b = 'b';
//	printf("%c%c\n", a, b);
//
//	wchar_t wc1 = L'王';
//	wchar_t wc2 = L'铮';
//	wprintf(L"%lc\n", wc1);
//	wprintf(L"%lc\n", wc2);
//	return 0;
//}


