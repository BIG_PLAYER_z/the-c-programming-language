#define _CRT_SECURE_NO_WARNINGS

#include<string.h>
#include<stdio.h>
#include<windows.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

void menu()
{
	printf("******************************\n");
	printf("***1.开始游戏    0.退出游戏***\n");
	printf("******************************\n");
}
void game()
{
	int ret = 0;
	ret = rand() % 100 + 1;
	int guess = 0;
	int count = 5;
	while (count)
	{
		printf("还剩下%d次机会\n", count);
		printf("猜数字：");
		scanf("%d", &guess);
		if (guess > ret)
		{
			printf("猜大了\n");
		}
		else if (guess < ret)
		{
			printf("猜小了\n");
		}
		else
		{
			printf("恭喜你！！！猜对了！！！\n");
			printf("\n\n");
			Sleep(1000);
			system("cls");
			break;
		}
		count--;
		if (count == 0)
		{
			printf("游戏失败\n");
			system("cls");
			break;
		}
	}
}
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择：");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}
	} while (input);
	return 0;
}
